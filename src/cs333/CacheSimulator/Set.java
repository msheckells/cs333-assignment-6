package cs333.CacheSimulator;

public class Set 
{
	private static int INVALID = -1;
	private Block[] blocks;
	private int blockSize;
	private int idCounter;
	private boolean writeAllocate;
	private boolean writeThrough;
	private boolean leastRecentlyUsed;
	private CacheSimulator cs;
	
	public Set(int numBlocks, int blockSize, boolean writeAllocate, boolean writeThrough, boolean leastRecentlyUsed, CacheSimulator cs)
	{
		this.blockSize = blockSize;
		this.writeAllocate = writeAllocate;
		this.writeThrough = writeThrough;
		this.leastRecentlyUsed = leastRecentlyUsed;
		this.cs = cs;
		blocks = new Block[numBlocks];
		
		idCounter = 0;
	}
	
	public boolean store(int tag){
		int nullIndex = INVALID;
		for(int i=0; i<blocks.length; i++){
			//Hit
			if(blocks[i] != null && blocks[i].getAddress()==tag){
				if(leastRecentlyUsed){
					blocks[i].setId(idCounter);
					idCounter++;
				}
				if(writeThrough){
					cs.updateCycles(101);
				}
				else{
					blocks[i].setDirty();
					cs.updateCycles(1);
				}
				return true;
			}
			//Get the index of the first empty slot in the set
			else if(blocks[i] == null && nullIndex == INVALID){
				nullIndex = i;
			}
		}
		//Miss
		if(writeAllocate){
			Block blk;
			//Set not full. Add new block to cache
			if(nullIndex != INVALID){
				blk = new Block(tag, blockSize, idCounter);
				blocks[nullIndex] = blk;
				idCounter++;
				cs.updateCycles(100*blockSize/4);
			}
			//Set full. Evict.
			else{
				blk = evictAndWrite(tag);
			}
			if(writeThrough){
				cs.updateCycles(101);
			}
			else{
				cs.updateCycles(1);
				blk.setDirty();
			}
		}
		//No write allocate
		else{
			cs.updateCycles(100);
		}
		return false;
	}
	
	public boolean load(int tag){
		int nullIndex = INVALID;
		for(int i=0; i<blocks.length; i++){
			//Hit. Load block from cache
			if(blocks[i] != null && blocks[i].getAddress()==tag){
				if(leastRecentlyUsed){
					blocks[i].setId(idCounter);
					idCounter++;
				}
				cs.updateCycles(1);
				return true;
			}
			else if(blocks[i] == null && nullIndex == -1){
				nullIndex=i;
			}
		}
		//Miss
		//Set not full. Load block from memory into cache. 
		if(nullIndex != -1){
			blocks[nullIndex] = new Block(tag, blockSize, idCounter);
			idCounter++;
			cs.updateCycles(1+100*blockSize/4);
		}
		//Set full. Evict.
		else{
			evictAndWrite(tag);
			cs.updateCycles(1);
		}
		return false;
	}
	
	//Evicts the block with the lowest id and writes a new block in its place
	//If leastRecentlyUsed then the lowest id will be the least recently used block
	//If FIFO then the lowest id will be the oldest block in the cache
	public Block evictAndWrite(int tag){
		int cycles=0;
		int min=Integer.MAX_VALUE;
		int minIndex=0;
		for(int i=0; i<blocks.length; i++){
			if(blocks[i].getId()<min){
				min = blocks[i].getId();
				minIndex = i;
			}
		}
		//Before evicting, if dirty, write to memory
		if(blocks[minIndex].isDirty()){
			cycles+=100*blockSize/4;
		}
		blocks[minIndex] = new Block(tag, blockSize, idCounter);
		idCounter++;
		cs.updateCycles(100*blockSize/4+cycles);
		return blocks[minIndex];
	}
	
	public String toString(){
		String output="";
		for(int i=0; i<blocks.length; i++){
			if(blocks[i]==null) output+="Empty\n";
			else output+="Address "+blocks[i].getAddress()+" ID "+blocks[i].getId()+"\n";
		}
		return output;
	}
	

}
