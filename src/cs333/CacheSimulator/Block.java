package cs333.CacheSimulator;

public class Block 
{
	private int blockSize;
	private int id;
	private int address;
	boolean dirty;
	
	public Block(int address, int blockSize, int id)
	{
		this.address = address;
		this.blockSize = blockSize;
		this.id	= id;
		dirty = false;
	}
	
	public int getAddress(){
		return address;
	}
	
	public int getSize(){
		return blockSize;
	}
	
	public void setDirty(){
		this.dirty=true;
	}
	
	public boolean isDirty(){
		return dirty;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public int getId(){
		return id;
	}
	
}
