package cs333.CacheSimulator;



import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;

public class CacheSimulator 
{
	private int blockSize;
	private int cycles;
	private Set[] sets;
	private int numLoads;
	private int numStores;
	private int loadHits;
	private int loadMisses;
	private int storeHits;
	private int storeMisses;
	
	public CacheSimulator(int numSets, int numBlocks, int blockSize, boolean writeAllocate, boolean writeThrough, boolean leastRecentlyUsed)
	{
		this.blockSize = blockSize;;
		cycles = 0;
		sets = new Set[numSets];
		numLoads = 0;
		numStores = 0;
		loadHits = 0;
		loadMisses = 0;
		storeHits = 0;
		storeMisses = 0;
		for(int i=0; i<numSets; i++)
		{
			sets[i] = new Set(numBlocks, blockSize, writeAllocate, writeThrough, leastRecentlyUsed, this);
		}
	}
	

	public void store(long byteAddress)
	{
		if(sets[mapToSet(byteAddress)].store(getTag(byteAddress)))
			storeHits++;
		else
			storeMisses++;
		numStores++;
	}
	
	public void load(long byteAddress)
	{
		if(sets[mapToSet(byteAddress)].load(getTag(byteAddress))){
			loadHits++;
		}
		else{
			loadMisses++;
		}
		numLoads++;
	}
	
	private int mapToSet(long address){
		String binary = Long.toBinaryString(address);
		int offsetBits = (int) (Math.log(blockSize)/Math.log(2));
		int setBits = (int) (Math.log(sets.length)/Math.log(2));
		if(binary.length()<=offsetBits+setBits) binary = "00000000000000000000000"+binary;
		binary = binary.substring(binary.length()-offsetBits-setBits, binary.length()-offsetBits);
		if(setBits==0) return 0;
		else return Integer.parseInt(binary, 2);
	}
	
	private int getTag(long address){
		String binary = Long.toBinaryString(address);
		int offsetBits = (int) (Math.log(blockSize)/Math.log(2));
		int setBits = (int) (Math.log(sets.length)/Math.log(2));
		if(binary.length()<=offsetBits+setBits) binary = "00000000000000000000000"+binary;
		binary = binary.substring(0, binary.length()-offsetBits-setBits);
		return Integer.parseInt(binary, 2);
	}
	
	public void updateCycles(int cycles){
		this.cycles+=cycles;
	}
	
	public int cycleCount(){
		return cycles;
	}
	
	public void printStats()
	{
		System.out.println("Total Loads: "+numLoads);
		System.out.println("Toatal Stores: "+numStores);
		System.out.println("Load hits: "+loadHits);
		System.out.println("Load misses: "+loadMisses);
		System.out.println("Store hits: "+storeHits);
		System.out.println("Store misses: "+storeMisses);
		System.out.println("Total cycles: "+cycles);
	}
	
	public static void main(String[] args)
	{
		
		if(args.length != 7)
		{
			System.out.println("Expected 7 arguments, got " + args.length + ".");
			return;
		}

		int numSets = Integer.parseInt(args[0]);
		int numBlocks = Integer.parseInt(args[1]);
		int blockSize = Integer.parseInt(args[2]);
		boolean useWriteAllocate = Integer.parseInt(args[3]) == 1 ? true : false;
		boolean useWriteThrough = Integer.parseInt(args[4]) == 1 ? true : false;
		boolean useLRU = Integer.parseInt(args[5]) == 1 ? true : false;
		String traceFile = args[6];
		
		if(numSets <= 0 || !numIsPowerOfTwo(numSets))
		{
			System.out.println("Number of sets in the cache must be a positive power of 2.");
			return;
		}
		
		if(numBlocks <= 0 || !numIsPowerOfTwo(numBlocks))
		{
			System.out.println("Number of blocks in each set must be a positive power of 2.");
			return;
		}
		
		if(blockSize < 4 || !numIsPowerOfTwo(blockSize))
		{
			System.out.println("Number of bytes in each block must be a positive power of 2 (at least 4).");
			return;
		}
		
		CacheSimulator cs = new CacheSimulator(numSets, numBlocks, blockSize, useWriteAllocate, useWriteThrough, useLRU);
		
		try
		{
			FileInputStream fstream = new FileInputStream(traceFile);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			
			String dataLine;
			while ((dataLine = br.readLine()) != null)   
			{
				String[] data = dataLine.split("\\s+");
				if(data[0].equals("l"))
				{
					//load
					cs.load(Long.parseLong(data[1].substring(2), 16));
				}
				else if(data[0].equals("s"))
				{
					//store
					cs.store(Long.parseLong(data[1].substring(2), 16));
					
				}
				else
				{
					System.out.println("Command \"" + data[0] + "\" not permitted. Shutting down simulation.");
					return;
				}
			}

			in.close();			
			cs.printStats();

		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static boolean numIsPowerOfTwo(int num)
	{
		return Math.log(num)/Math.log(2) == (int)(Math.log(num)/Math.log(2));
	}

	
}
